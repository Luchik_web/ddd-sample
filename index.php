<?php

use Domain\Specifications\Impl\Spec;

$spec = Spec::In('aaa', 'bbb', array(5,6))
                ->AndWhere(Spec::Equal('aaa', 'ccc', 1))
                ->Contains('ss', 'ff', '%ab%');

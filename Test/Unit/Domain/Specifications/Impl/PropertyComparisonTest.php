<?php

namespace Test\Unit\Specifications\Impl;

use Domain\Specifications\Impl\PropertyComparison;

require_once 'MockedEntity.php';

class PropertyComparisonTest  extends \PHPUnit_Framework_TestCase {
    
    /**
     * @test
     */
    function IsSatisfiedBy_can_access_private_field_of_entity(){
        $lookupValue = 'abcd';
        $obj = new MockedEntity(1);
        $obj->setFieldValue($lookupValue);
        $propName = $obj->getFieldName();
        
        $operation = 
                $this->getMockBuilder('Domain\Specifications\EnumComparisonOperation', array('IsSatisfiedBy'))
                ->disableOriginalConstructor()
                ->getMock();
        
        $operation->expects($this->once())
                ->method('IsSatisfiedBy')
                ->with($this->equalTo($lookupValue), $this->equalTo($lookupValue))
                ->will($this->returnValue(true));

        $expr = new PropertyComparison($propName, $operation, $lookupValue);

        $expr->IsSatisfiedBy($obj);
    }
}

<?php

namespace Test\Unit\Specifications\Impl;
use Domain\Entities\AbstractEntity;

class MockedEntity extends AbstractEntity {
    
    private $privField;
    protected $protField;
    public $publField;
    
    private $level;
    
/**
 * Defines to which property setter would assign value and getter would return
 * @param integer $fieldEncapsulationLevel 1-private, 2-protected, 3 public. 
 * 
 */
    public function __construct($fieldEncapsulationLevel) {
        $this->level = $fieldEncapsulationLevel;
        
        if($this->level<1 || $this->level>3)
            throw new Exception('wrong filed encapsulation level was passed to constructor');
    }

    public function setFieldValue($value) {
        switch ($this->level){
            case 1: $this->privField = $value; break;
            case 2: $this->protField = $value; break;
            case 3: $this->publField = $value; break;
        }
    }

    
    public function getFieldName() {
        switch ($this->level) {
            case 1: return 'privField';
            case 2: return 'protField';
            case 3: return 'publField';
        }
    }
    
    public function getFieldEncapsulation() {
        switch ($this->level) {
            case 1: return 'private field';
            case 2: return 'protected field';
            case 3: return 'public filed';
        }
    }

}


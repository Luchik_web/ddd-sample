<?php

namespace Test\Unit\Specifications\Impl;

use Domain\Specifications\Impl\Spec;
use Domain\Specifications\EnumComparisonOperation;
use Domain\Specifications\EnumLogicalCondition;

require_once 'MockedEntity.php';

class SpecTest extends \PHPUnit_Framework_TestCase {

     /**
     * @test
     */
    public function CreateExpression_creates_instance_of_Spec_type() {

        $propertyName = 'prop1';
        $compareWith = EnumComparisonOperation::EQ();
        $possibleValue = 5;

        $spec = Spec::CreateExpression($propertyName, $compareWith, $possibleValue);

        $this->assertInstanceOf('Domain\Specifications\Impl\Spec', $spec, 'CreateExpression method had failed to create specification of correct type.');
    }
    
    /**
     * @test
     */
    public function CreateExpression_creates_correct_leaf_expression() {

        $propertyName = 'prop1';
        $compareWith = EnumComparisonOperation::EQ();
        $possibleValue = 5;
        
        $expr = Spec::CreateExpression($propertyName, $compareWith, $possibleValue)
                ->getLeafExpression();

        $this->assertSame($compareWith, $expr->getPropertyOperation(),
                'CreateExpression method had failed to create expression with correct comparison operation.');

        $this->assertEquals($propertyName, $expr->getProperty(), 
                'CreateExpression method had failed to create expression with correct property name.');

        $this->assertSame($possibleValue, $expr->getPropertyValues(), 
                'CreateExpression method had failed to create expression with correct values.');
    }

     /**
     * @test
     */
    public function CreateExpression_creates_expression_with_no_left_and_right_legs() {

        $propertyName = 'prop1';
        $compareWith = EnumComparisonOperation::EQ();
        $possibleValue = 5;

        $spec = Spec::CreateExpression($propertyName, $compareWith, $possibleValue);

        $this->assertNull($spec->getLeftSubtree(), 
                'CreateExpression has failed by creating a specification with left leg.');
    
        $this->assertNull($spec->getRightSubtree(), 
                'CreateExpression has failed by creating a specification with right leg.');

        $this->assertEquals(EnumLogicalCondition::_LEAF_, $spec->getLogicalCondition()->value(), 
                'CreateExpression has failed to mark expression as leaf.');
    }

     /**
     * @test
     */
    public function AndWhere_composes_two_specification_into_single_one_with_AND_logical_operation() {

        $propertyName = 'prop1';
        $compareWith = EnumComparisonOperation::EQ();
        $possibleValue = 5;

        $spec = Spec::CreateExpression($propertyName, $compareWith, $possibleValue);

        $this->assertInstanceOf('Domain\Specifications\Impl\Spec', $spec, 'CreateExpression method had failed to create specification of correct type.');
    }
}


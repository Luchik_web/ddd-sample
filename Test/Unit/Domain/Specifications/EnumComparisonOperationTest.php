<?php

namespace Test\Unit\Specifications;

use Domain\Specifications\EnumComparisonOperation;

class EnumComparisonOperationTest extends \PHPUnit_Framework_TestCase {
    
    /**
     * 
     * @test
     * @dataProvider getValidComparisons
     */
    function IsSatisfiedBy_method_compares_scalar_values_correctly($leftValue, $rightValue, EnumComparisonOperation $operation) {
        
        $opName = $operation->value();
        
        $errMessage = "Operation '$opName' failed to compare value '$leftValue' to value '$rightValue'.";
        
        $this->assertTrue( $operation->IsSatisfiedBy($leftValue, $rightValue), $errMessage);
    }
    
    function getValidComparisons() {
        return array(
            array(1234, 1234, EnumComparisonOperation::EQ()),
            array(12.34, 12.34, EnumComparisonOperation::EQ()),
            array(-1234, -1234, EnumComparisonOperation::EQ()),
            array('a234', 'a234', EnumComparisonOperation::EQ()),
            array(true, true, EnumComparisonOperation::EQ()),
            
            array(1234, 234, EnumComparisonOperation::NEQ()),
            array(12.34, 2.34, EnumComparisonOperation::NEQ()),
            array(-1234, 1234, EnumComparisonOperation::NEQ()),
            array('a234', 'b234', EnumComparisonOperation::NEQ()),
            array(true, false, EnumComparisonOperation::NEQ()),
            
            array(1234, 1233, EnumComparisonOperation::GT()),
            array(-1234, -1235, EnumComparisonOperation::GT()),
            array('abcde', 'abcd', EnumComparisonOperation::GT()),
            array(0.1, 0.01, EnumComparisonOperation::GT()),
            
            array(1234, 1233, EnumComparisonOperation::GTE()),
            array(1234, 1234, EnumComparisonOperation::GTE()),
            array(-1234, -1235, EnumComparisonOperation::GTE()),
            array(-1234, -1234, EnumComparisonOperation::GTE()),
            array('abcde', 'abcd', EnumComparisonOperation::GTE()),
            array('abcd', 'abcd', EnumComparisonOperation::GTE()),
            array(0.1, 0.01, EnumComparisonOperation::GTE()),
            array(0.1, 0.1, EnumComparisonOperation::GTE()),

            array(1233, 1234, EnumComparisonOperation::LT()),
            array(-1235, -1234, EnumComparisonOperation::LT()),
            array('abcd', 'abcde', EnumComparisonOperation::LT()),
            array(0.01, 0.1, EnumComparisonOperation::LT()),
            
            array(1233, 1234, EnumComparisonOperation::LTE()),
            array(1234, 1234, EnumComparisonOperation::LTE()),
            array(-1235, -1234, EnumComparisonOperation::LTE()),
            array(-1234, -1234, EnumComparisonOperation::LTE()),
            array('abcd', 'abcde', EnumComparisonOperation::LTE()),
            array('abcd', 'abcd', EnumComparisonOperation::LTE()),
            array(0.01, 0.1, EnumComparisonOperation::LTE()),
            array(0.1, 0.1, EnumComparisonOperation::LTE()),
            
            array(1234, array(1233,1234), EnumComparisonOperation::IN()),
            array(-1234, array(-1235, -1234, 1234), EnumComparisonOperation::IN()),
            array(-1234, array(-1235, '-1234', 1234), EnumComparisonOperation::IN()),
            array(-123.4, array(-1235, '-123.4', 1234), EnumComparisonOperation::IN()),
            array(-123.4, array(-1235, '-123', -123.4), EnumComparisonOperation::IN()),

            array(1234, array(1233, 1231), EnumComparisonOperation::NOTIN()),
            array(1234, array(), EnumComparisonOperation::NOTIN()),
            array(-1234, array(1234, '1234'), EnumComparisonOperation::NOTIN()),
            array(-123.4, array(-1234, '-123.3999', -123.3999), EnumComparisonOperation::NOTIN()),
            
            array(null, array(1233, 1231), EnumComparisonOperation::ISNULL()),
            array(null, null, EnumComparisonOperation::ISNULL()),
            array(null, 5, EnumComparisonOperation::ISNULL()),
            
            array('a234', '234', EnumComparisonOperation::CONTAINS()),
            array('a234', 'a234', EnumComparisonOperation::CONTAINS()),
            array('a234', 'a23', EnumComparisonOperation::CONTAINS()),
            array('a234', '23', EnumComparisonOperation::CONTAINS())
        );
    }
    
}


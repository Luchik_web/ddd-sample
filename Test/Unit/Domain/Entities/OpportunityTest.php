<?php

namespace Test\Unit\Domain\Entities;
use Domain\Entities\Opportunity;
use Domain\Values;

class OpportunityTest extends \PHPUnit_Framework_TestCase {
    
    /**
     * @test
     */
    public function constructor_should_initialize_fields_to_default_values(){


        $user = $this->getUserMock();
        $org = $this->getOrganisationMock();

        $opportunity = new Opportunity($user, $org);
        
        $this->assertEquals(Values\EnumOpportunityStatus::OFFLINE(), 
                $opportunity->getStatus(), 'Wrong initialization of status');
        
        $this->assertEmpty($opportunity->getTitle(), 'Wrong initialization of title.');
        $this->assertEmpty($opportunity->getDescription(), 'Wrong initialization of description.');
        $this->assertTrue($opportunity->isEnabled(), 'Wrong initialization of enabled flag.');
        $this->assertFalse($opportunity->isMarkedAsDeleted(), 'Wrong initialization of deleted flag.');
        $this->assertTrue($opportunity->isAcceptingUsersFromAnyOrganizations(), 
                'Wrong initialization of is_for_organisation_only field.');
        $this->assertEquals(0, $opportunity->getMaxJobs(), 'Wrong initialization of max jobs.');
        $this->assertEquals(0, $opportunity->getRecurringDays(), 'Wrong initialization of reccuring days.');
        
        $expectedPeriod = new Values\TimePeriod(null, null);
        $this->assertEquals($expectedPeriod, $opportunity->getTimePeriod(), 
                'Wrong initialization of start or end dates.');

        $expectedAmount = new Values\Amount(0.0, \Domain\Values\EnumCurrency::VIVO());
        $this->assertEquals($expectedAmount, $opportunity->getAmount(), 'Wrong initialisation of amount or currency.');
        
        $expectedLocation = new Values\GeoLocation(0.0, 0.0, FALSE);
        $this->assertEquals($expectedLocation, $opportunity->getGeoLocation(), 
                'Wrong initialization of longitude or latitude.');
        
        $this->assertNull($opportunity->getEventDate(), 'Wrong initialization of event date.');
        $this->assertSame($org, $opportunity->getOrganisation(), 'Wrong assignment of organisation.');
        $this->assertSame($user, $opportunity->getCreator(), 'Wrong assignment of creator.');
    }
    
    /**
     * 
     * @test
     */
    public function geoLocation_can_be_read_after_assignment() {
       $location = new Values\GeoLocation(1.0, 5.0, TRUE);
       $opportinity = $this->getOpportunity();
       $opportinity->setGeoLocation($location);
       
       $this->assertEquals($location, $opportinity->getGeoLocation(), 'Geo location can not be read back after assignment.');
    }
    
    
    private function getOpportunity(){
        return new Opportunity($this->getUserMock(), $this->getOrganisationMock());
    }
    
    private function getUserMock() {
        return $this->getMockBuilder('Domain\Entities\User')
                        ->disableOriginalConstructor()
                        ->getMock();
    }

    private function getOrganisationMock() {
        return $this->getMockBuilder('Domain\Entities\Organisation')
                        ->disableOriginalConstructor()
                        ->getMock();
    }

}


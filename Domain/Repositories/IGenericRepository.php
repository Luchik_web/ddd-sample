<?php

namespace Domain\Repositories;

use Domain\Entities\AbstractEntity;
use Domain\Specifications\ISpecificationTree;
use Doctrine\Common\Collections\Collection;//possibly SPL will introduce this interface in future release. 

interface IGenericRepository {

    /**
     * Get single entity by primary key
     * 
     * @param string $entityClass
     * @param integer $Id
     * @return AbstractEntity
     */
    function GetById($entityClass, $Id);
    
    /**
     * Get collection of entities filtered with specification
     * 
     * @param string $entityClass class name
     * @param ISpecificationTree $specification filtering condition 
     * @return Collection collection of entities of the required type
     */
    function GetBySpec($entityClass, ISpecificationTree $specification);
    
    /**
     * Adding new entity to repository and eventually to database
     * 
     * @param AbstractEntity $entity
     */
    function Save(AbstractEntity $entity);
    
     /**
     * Removing existing entity from repository and eventually from database
     * 
     * @param AbstractEntity $entity
     */
    function Remove(AbstractEntity $entity);
}


<?php

namespace Domain\Repositories;

interface ITransaction {
    function BeginTransaction();
    function Commit();
    function Rollback();
}

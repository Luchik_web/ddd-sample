<?php
namespace Domain\Entities;

abstract class AbstractEntity implements IKey, IVersioning {
    
    protected $id;
    protected $createdAt;
    protected $updatedAt;
    
    function __construct() {
        $this->createdAt = $this->updatedAt = new \DateTime();
        $this->version = 0;
        $this->id = 0;
    }

    function getId(){return $this->id;}
    final function getCreatedAt() {return $this->createdAt;}

    /**
     * Version field. Contains timestamp of the last update operation
     * @return type
     */
    final function getUpdatedAt() {return $this->updatedAt;}
}
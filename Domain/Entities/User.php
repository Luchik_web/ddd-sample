<?php
namespace Domain\Entities;

use Domain\Values\EnumGender;
use Domain\Values\EnumUserStatus;

class User extends AbstractEntity {

    function __construct() {
        $this->setFname('');
        $this->setSname('');
        $this->setTitle('Mr');
        $this->setEmail(null);
        $this->setMobile(null);
        $this->setDob(null);
        $this->setLastLogonTime(null);
        $this->setPasswordResetHash(null);
        $this->setSex(EnumGender::MALE());
        $this->setStatus(EnumUserStatus::NOACTIVE());
    }
    
    public function getFname() {
        return $this->fname;
    }

    public function setFname($fname) {
        $this->fname = $fname;
    }

    public function getSname() {
        return $this->sname;
    }

    public function setSname($sname) {
        $this->sname = $sname;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getMobile() {
        return $this->mobile;
    }

    public function setMobile($mobile) {
        $this->mobile = $mobile;
    }

    /**
     * 
     * @return \DateTime
     */
    public function getDob() {
        return $this->dob;
    }

    public function setDob(\DateTime $dob=null) {
        $this->dob = $dob;
    }

     /**
     * 
     * @return \DateTime
     */
    public function getLastLogonTime() {
        return $this->last_logon_at;
    }

    public function setLastLogonTime(\DateTime $last_logon_at=null) {
        $this->last_logon_at = $last_logon_at;
    }

    public function getPasswordResetHash() {
        return $this->password_reset_hash;
    }

    public function setPasswordResetHash($password_reset_hash) {
        $this->password_reset_hash = $password_reset_hash;
        
        $utcTime = null;
        if($password_reset_hash != null)
            $utcTime = new \DateTime();
        
        $this->created_hash = $utcTime;
    }

    /**
     * 
     * @return \DateTime
     */
    public function getPasswordResetHashTime() {
        return $this->created_hash;
    }

    /**
     * 
     * @return EnumGender
     */
    public function getSex() {
        return EnumGender::memberByValue($this->sex);
    }

    public function setSex(EnumGender $sex) {
        $this->sex = $sex->value();
    }

    /**
     * 
     * @return EnumUserStatus
     */
    public function getStatus() {
        return EnumUserStatus::memberByValue($this->status);
    }

    public function setStatus(EnumUserStatus $status) {
        $this->status = $status->value();
    }

    
    private $fname;
    private $sname;
    private $title;
    private $email;
    private $mobile;
    private $dob;
    private $last_logon_at;
    private $password_reset_hash;
    private $created_hash;
    private $sex;
    private $status;
}


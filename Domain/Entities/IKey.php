<?php
namespace Domain\Entities;

interface IKey {
    function getId();
}

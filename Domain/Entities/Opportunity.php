<?php

namespace Domain\Entities;

use Domain\Values\GeoLocation;
use Domain\Values\Amount;
use Domain\Values\TimePeriod;
use Domain\Values\EnumCurrency;
use Domain\Values\EnumOpportunityStatus;


class Opportunity extends AbstractEntity {
    
    /**
     * 
     * @param \Domain\Entities\User $user
     * @param \Domain\Entities\Organisation $organisation
     */
    public function __construct(User $user, Organisation $organisation){
       
       $this->setStatus(EnumOpportunityStatus::OFFLINE());
        
       $this->setTitle('');
       $this->setDescription('');
       
       $this->enable();
       $this->markAsLive();
       
       $this->acceptUsersFromAnyOrganization();

       $this->setMaxJobs(0);
       $this->setRecurringDays(0);
       $this->setTimePeriod(new TimePeriod(null, null));

       $this->setAmount(new Amount(0.0, EnumCurrency::VIVO()));
       
       $this->setGeoLocation( new GeoLocation(0.0, 0.0, FALSE) );
       
       $this->setEventDate(null);
       
       $this->setOrganisation($organisation);
       $this->setCreator($user);
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * 
     * @return boolean
     */
    public function isEnabled() {
        return $this->enable;
    }

    public function enable() {
        $this->enable = true;
    }

    public function disable() {
        $this->enable = false;
    }

    /**
     * 
     * @return boolean
     */
    public function isMarkedAsDeleted() {
        return $this->delete;
    }

    public function markAsDeleted() {
        $this->delete = true;
    }

    /**
     * In UI there is not way for user to undeleted once deleted opportunity.
     * Therefore, this method is used by __construct only.
     */
    protected function markAsLive() {
        $this->delete = false;
    }

    /**
     * 
     * @return EnumOpportunityStatus
     */
    public function getStatus() {
        return EnumOpportunityStatus::memberByValue($this->status);
    }

    public function setStatus(EnumOpportunityStatus $status) {
        $this->status = $status->value();
    }

     /**
     * 
     * @return boolean
     */
    public function isAcceptingUsersFromAnyOrganizations() {
        return !$this->is_for_organisation_only;
    }

    public function acceptUsersFromOwnOrganization() {
        $this->is_for_organisation_only = true;
    }

    public function acceptUsersFromAnyOrganization() {
        $this->is_for_organisation_only = false;
    }

    public function getMaxJobs() {
        return $this->max_jobs;
    }

    public function setMaxJobs($max_jobs) {
        $this->max_jobs = $max_jobs;
    }

    public function getRecurringDays() {
        return $this->recurring_days;
    }

    public function setRecurringDays($recurring_days) {
        $this->recurring_days = $recurring_days;
    }

    /**
     * 
     * @return Amount
     */
    public function getAmount() {
        $currency = EnumCurrency::memberByValue($this->currency);
        return new Amount($this->cost, $currency);
    }

    public function setAmount(Amount $amount) {
        $this->cost = $amount->getValue();
        $this->currency = $amount->getCurrency()->value();
    }

    /**
     * 
     * @return GeoLocation
     */
    public function getGeoLocation() {
        return new GeoLocation($this->latitude, $this->longitude, $this->is_enabled_geo_location);
    }

    public function setGeoLocation(GeoLocation $location) {
        $this->latitude = $location->getLatitude();
        $this->longitude = $location->getLongitude();
        $this->is_enabled_geo_location = $location->IsEnabled();
    }

    /**
     * 
     * @return \DateTime
     */
    public function getEventDate() {
        return $this->event_date;
    }

    public function setEventDate(\DateTime $event_date=null) {
        $this->event_date = $event_date;
    }

    /**
     * 
     * @return TimePeriod
     */
    public function getTimePeriod() {
        return new TimePeriod($this->start_time, $this->end_time);
    }

    public function setTimePeriod(TimePeriod $period) {
        $this->start_time = $period->getStartTime();
        $this->end_time = $period->getEndTime();
    }

    /**
     * 
     * @return Organisation
     */
    public function getOrganisation() {
        return $this->organisation;
    }

    public function setOrganisation(Organisation $organisation) {
        $this->organisation = $organisation;
    }

    /**
     * 
     * @return User
     */
    public function getCreator() {
        return $this->creator;
    }
   
    public function setCreator(User $user) {
        $this->creator = $user;
    }


    private $title;
    private $description;
    private $enable;
    private $delete;
    private $status;
    private $is_enabled_geo_location;
    private $is_for_organisation_only;
    private $max_jobs;
    private $recurring_days;
    private $cost;
    private $latitude;
    private $longitude;
    private $event_date;
    private $start_time;
    private $end_time;
    private $currency;
    private $organisation;
    private $creator;

}

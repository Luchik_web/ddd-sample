<?php

namespace Domain\Entities;

interface IVersioning {
    /**
     * @return datetime time when object was inserted into database
     */
    function getCreatedAt();
    
    /**
     * @return datetime time when object was updated in database. Used for optimistic locks
     */
    function getUpdatedAt();
}

<?php
namespace Domain\Entities;

use Domain\Values\Amount;
use Domain\Values\EnumCurrency;
use Domain\Values\EnumOrgCategory;
use Domain\Values\EnumOrgStatus;
use Domain\Values\EnumOrgPrivacy;
use Domain\Values\EnumOrgMemberInvite;

class Organisation extends AbstractEntity {
    
    function __construct() {

        $this->setDomain('');
        $this->setName('');
        $this->setBudget(new Amount(0.0, EnumCurrency::POUND()));      
        $this->setSurplus(0.0);
        
        $this->setPrivacy(EnumOrgPrivacy::_PRIVATE());
        $this->setMembersInvitationType(EnumOrgMemberInvite::ADD());
        $this->setStatus(EnumOrgStatus::_NEW());
        $this->setCategory(EnumOrgCategory::SCHOOL()); 
    }

    public function getDomain() {
        return $this->domain;
    }

    public function setDomain($domain) {
        $this->domain = $domain;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    /**
     * 
     * @return EnumOrgPrivacy
     */
    public function getPrivacy() {
        return EnumOrgPrivacy::memberByValue($this->privacy);
    }

    public function setPrivacy(EnumOrgPrivacy $privacy) {
        $this->privacy = $privacy->value();
    }

    /**
     * 
     * @return Amount
     */
    public function getBudget() {
        return new Amount($this->budget, EnumCurrency::POUND());
    }

    public function setBudget(Amount $budget) {
        if( $budget->getCurrency() != EnumCurrency::POUND() )
            throw new Exception('Organization budget must be in pounds');
        
        $this->budget = $budget->getValue();
    }

     /**
     * 
     * @return Amount
     */
    public function getSurplus() {
        return $this->surplus;
    }

    public function setSurplus(Amount $surplus) {
        if ($surplus->getCurrency() != EnumCurrency::POUND())
            throw new Exception('Organization surplus must be in pounds');

        $this->surplus = $surplus->getValue();
    }

    /**
     * 
     * @return EnumOrgMemberInvite
     */
    public function getMembersInvitationType() {
        return EnumOrgMemberInvite::memberByValue($this->members_adding);
    }

    public function setMembersInvitationType(EnumOrgMemberInvite $members_adding) {
        $this->members_adding = $members_adding->value();
    }

    /**
     * 
     * @return EnumOrgStatus
     */
    public function getStatus() {
        return EnumOrgStatus::memberByValue($this->status);
    }

    public function setStatus(EnumOrgStatus $status) {
        $this->status = $status->value();
    }

    /**
     * 
     * @return EnumOrgCategory
     */
    public function getCategory() {
        return EnumOrgCategory::memberByValue($this->category);
    }

    public function setCategory(EnumOrgCategory $category) {
        $this->category = $category->value();
    }
    
        
    private $domain;
    private $name;
    private $privacy;
    private $budget;
    private $surplus;
    private $members_adding;
    private $status;
    private $category;

}


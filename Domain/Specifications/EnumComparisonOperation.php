<?php

namespace Domain\Specifications;

use Eloquent\Enumeration\Enumeration;

class EnumComparisonOperation extends Enumeration {
    const EQ = '=';
    const NEQ = '<>';
    const LT = '<';
    const LTE = '<=';
    const GT = '>';
    const GTE = '>=';
    const IN = 'IN';
    const NOTIN = 'NOT IN';
    const CONTAINS = 'CONTAINS';
    const ISNULL = 'IS NULL';
//    const ISNOTNULL = 'IS NOT NULL';
    
    /**
     * 
     * @param scalar $leftValue
     * @param scalar|array $rightValue
     * @return type
     * @throws Exception
     */
    public function IsSatisfiedBy($leftValue, $rightValue){
        
        switch ($this->value()) {

            case self::EQ : 
                return $leftValue == $rightValue;
                break;
            
            case self::NEQ :
                return $leftValue != $rightValue;
                break;
            
            case self::GT : 
                return $leftValue > $rightValue;
                break;
            
            case self::GTE : 
                return $leftValue >= $rightValue;                
                break;
            
            case self::LT : 
                return $leftValue < $rightValue;
                break;
            
            case self::LTE : 
                return $leftValue <= $rightValue;
                break;

            case self::IN :
                return in_array($leftValue, $rightValue);
                break;

            case self::NOTIN : 
                return !in_array($leftValue, $rightValue);
                break;

            case self::CONTAINS :
                return strpos($leftValue, $rightValue) !== FALSE;
                break;
            
            case self::ISNULL :
                return $leftValue == NULL;
                break;

//            case self::ISNOTNULL :
//                return $leftValue != NULL;
//                break;



            default:
                throw new \Exception('Unknown enumerator value');
        }        
    }
    
}

<?php
namespace Domain\Specifications;

interface IPropertyComparison extends ISpecification {

    
    function getProperty();
    
    /**
     * @return EnumComparisonOperation
     */
    function getPropertyOperation();
    
    /**
     *@return array string values the expression works on
     */
    function getPropertyValues();
}
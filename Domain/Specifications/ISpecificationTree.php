<?php

namespace Domain\Specifications;

interface ISpecificationTree {
      
    /**
    * @return ISpecificationTree
    */
    public function getLeftSubtree();

    /**
    * @return ISpecificationTree
    */
    public function getRightSubtree();
    
    /**
    * @return EnumLogicalCondition
    */
    public function getLogicalCondition();
    
    /**
     * @return IPropertyComparison returns non null property expression only for leaf nodes
     */
    public function getLeafExpression();
      
}

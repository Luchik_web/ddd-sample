<?php

namespace Domain\Specifications;

use Domain\Entities\AbstractEntity;

interface ISpecification {
    /**
     * Checks if entity satisfies logical condition defined by specification
     * 
     * @param \Domain\Entities\AbstractEntity $entity
     * @return boolean
     */
    public function IsSatisfiedBy(AbstractEntity $entity);
}

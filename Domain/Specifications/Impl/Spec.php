<?php

namespace Domain\Specifications\Impl;

use Domain\Entities\AbstractEntity;
use Domain\Specifications\ISpecificationTree;
use Domain\Specifications\ISpecification;
use Domain\Specifications\IPropertyComparison;
use Domain\Specifications\EnumLogicalCondition;
use Domain\Specifications\EnumComparisonOperation;

class Spec implements ISpecificationTree, ISpecification {

    /**
     * Factory creating simple specification containing only single comparison expression.
     * 
     * @param string $propertyName name of object property to create expression against
     * @param EnumComparisonOperation $compareWith operation used for property comparison
     * @param scalar|array of scalars $possibleValue used to compare existing property value aganst this
     * @return Spec
     */
    public static function CreateExpression($propertyName, EnumComparisonOperation $compareWith, $possibleValue) {

        $pe = new PropertyComparison($propertyName, $compareWith, $possibleValue);
        $spec = new Spec(EnumLogicalCondition::_LEAF_(), null, null);
        $spec->SetLeafExpression($pe);

        return $spec;
    }

    /**
     *  @param Spec $spec
     *  @return Spec
     */
    public function AndWhere(Spec $spec) {
        return new Spec(EnumLogicalCondition::_AND_(), $this, $spec);
    }

    /**
     *  @param Spec $spec
     *  @return Spec
     */
    public function OrWhere(Spec $spec) {
        return new Spec(EnumLogicalCondition::_OR_(), $this, $spec);
    }

    /**
     *  @param Spec $spec
     *  @return Spec
     */
    public function NotWhere() {
        return new Spec(EnumLogicalCondition::_NOT_(), $this, null);
    }

    public function getLeftSubtree() {
        return $this->left;
    }

    public function getRightSubtree() {
        return $this->right;
    }

    public function getLogicalCondition() {
        return $this->logicalCondition;
    }

    public function getLeafExpression() {
        return $this->leafExpression;
    }

    public function IsSatisfiedBy(AbstractEntity $entity) {

        if ($this->leafExpression) {
            return $this->leafExpression->IsSatisfiedBy($entity);
        }
        
        switch ($this->logicalCondition->key()) {
            case EnumLogicalCondition::_AND_ :

                return $this->left->IsSatisfiedBy($entity) && $this->right->IsSatisfiedBy($entity);
                break;

            case EnumLogicalCondition::_OR_ :

                return $this->left->IsSatisfiedBy($entity) || $this->right->IsSatisfiedBy($entity);
                break;

            case EnumLogicalCondition::_NOT_ :

                return !$this->left->IsSatisfiedBy($entity);
                break;

            default:
                break;
        }
    }

    private function setLeafExpression(IPropertyComparison $expr) {
        $this->leafExpression = $expr;
        $this->logicalCondition = EnumLogicalCondition::_LEAF_();
        $this->left = $this->right = null;
    }    

  
    private $logicalCondition;
    private $left;
    private $right;

    /**
     *
     * @var IPropertyComparison 
     */
    private $leafExpression;

    protected function __construct(EnumLogicalCondition $logicalCondition, Spec $left = null, Spec $right = null) {
        $this->logicalCondition = $logicalCondition;
        $this->left = $left;
        $this->right = $right;
    }

}


<?php

namespace Domain\Specifications\Impl;
use Domain\Specifications\EnumComparisonOperation;
use Domain\Specifications\IPropertyComparison;
use Domain\Entities\AbstractEntity;

class PropertyComparison implements IPropertyComparison {

    private $property;
    private $propertyOperation;
    private $propertyValues;

    /**
     * 
     * @param string $property
     * @param EnumComparisonOperation $propertyOperation
     * @param scalar|array of scalars $propertyValues
     */
    function __construct($property, EnumComparisonOperation $propertyOperation, $propertyValues) {

        $this->property = $property;
        $this->propertyOperation = $propertyOperation;
        $this->propertyValues = $propertyValues;
    }

    public function getPropertyOperation() {
        return $this->propertyOperation;
    }

    public function getPropertyValues() {
        return $this->propertyValues;
    }

    public function getProperty() {
        return $this->property;
    }

    public function IsSatisfiedBy(AbstractEntity $entity) {

        $refl = new \ReflectionObject($entity);
        $prop = $refl->getProperty($this->property);
        if(!$prop) return FALSE;
        
        $prop->setAccessible( true );
        $entityValue = $prop->getValue($entity);
        
        return $this->propertyOperation->IsSatisfiedBy($entityValue, $this->propertyValues);
        
    }
}


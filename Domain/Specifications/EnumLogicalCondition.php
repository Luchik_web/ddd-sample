<?php

namespace Domain\Specifications;

use Eloquent\Enumeration\Enumeration;

class EnumLogicalCondition extends Enumeration {
    
    const _AND_ = 'AND';
    const _OR_  = 'OR';
    
    /**
     * Leaf node in syntax tree not having any subtrees under it
     */
    const _LEAF_ = '';
}

<?php
namespace Domain\Values;
use Eloquent\Enumeration\Enumeration;

class EnumUserStatus extends Enumeration{
   const ENABLE = 1;
   const DISABLE = 2;
   const SUSPEND = 3;
   const NOACTIVE = 4;
}

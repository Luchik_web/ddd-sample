<?php
namespace Domain\Values;
use Eloquent\Enumeration\Enumeration;

class EnumOrgPrivacy extends Enumeration{
   const _PRIVATE = 1;
   const _PUBLIC = 2;
}

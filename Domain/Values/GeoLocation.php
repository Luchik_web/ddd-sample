<?php
namespace Domain\Values;

class GeoLocation {
    
    function __construct($latitude, $longitude, $enabled=true) {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->enabled = $enabled;
    }

    /**
     * 
     * @return float
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * 
     * @return Currency
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     * 
     * @return boolean
     */
    public function IsEnabled(){
        return $this->enabled;
    }
            


    private $latitude;
    private $longitude;
    private $enabled;
}

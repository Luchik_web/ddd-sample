<?php

namespace Domain\Values;

class Amount {
    
    function __construct($value, EnumCurrency $currency) {
        $this->value = $value;
        $this->currency = $currency;
    }
      
    /**
     * 
     * @return float
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * 
     * @return EnumCurrency
     */
    public function getCurrency() {
        return $this->currency;
    }
    
    public function equals($param) {
        
    }




    /**
     * 
     * @param EnumCurrency $currency
     * @return Amount amount in required currency
     */
    public function ConvertTo(EnumCurrency $currency, $currencyRate = 1.0){
        return new Amount($this->value * $currencyRate, $currency);
    }

    private $value;
    private $currency;
}

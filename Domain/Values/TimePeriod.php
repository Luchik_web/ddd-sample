<?php

namespace Domain\Values;

class TimePeriod {
    function __construct(\DateTime $startTime=null, \DateTime $endTime=null) {
        $this->startTime = $startTime;
        $this->endTime = $endTime;
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getStartTime() {
        return $this->startTime;
    }

    /**
     * 
     * @return \DateTime
     */
    public function getEndTime() {
        return $this->endTime;
    }

    
    private $startTime;
    private $endTime;
    
}

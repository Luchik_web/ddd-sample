<?php
namespace Domain\Values;
use Eloquent\Enumeration\Enumeration;

class EnumOrgMemberInvite extends Enumeration{
   const ADD = 1;
   const INVITE = 2;
}

<?php

namespace Domain\Values;
use Eloquent\Enumeration\Enumeration;

final class EnumGender extends Enumeration{
    
    const MALE = 1;
    const FEMALE = 2;
}

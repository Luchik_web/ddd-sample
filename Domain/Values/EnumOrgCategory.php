<?php

namespace Domain\Values;
use Eloquent\Enumeration\Enumeration;

final class EnumOrgCategory extends Enumeration{
    
    const SYSTEM = 1;
    const CHARITY = 2;
    const SPONSOR = 3;
    const CORPORATE = 4;
    const SCHOOL = 5;
    const FE_COLLEGE = 6;
    const PCT = 7;
    const SOCIAL_ENTERPRISE = 8;
    const LOCAL_AUTHORITY = 9;   
}

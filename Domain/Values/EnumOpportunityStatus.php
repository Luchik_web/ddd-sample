<?php
namespace Domain\Values;
use Eloquent\Enumeration\Enumeration;

class EnumOpportunityStatus extends Enumeration{
   const OFFLINE = 1;
   const LIMIT = 2;
   const UNLIMIT = 3;
}

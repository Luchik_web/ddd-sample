<?php

namespace Domain\Values;
use Eloquent\Enumeration\Enumeration;

class EnumCurrency extends Enumeration {
    const VIVO = 1;
    const POUND = 2;
    const VIVO_MONETARY = 3;
}


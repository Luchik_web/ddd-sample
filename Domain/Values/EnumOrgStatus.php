<?php
namespace Domain\Values;
use Eloquent\Enumeration\Enumeration;

class EnumOrgStatus extends Enumeration{
   const _NEW = 1;
   const PENDING = 2;
   const ACTIVE = 3;
}

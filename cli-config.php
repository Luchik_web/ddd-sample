<?php
// replace with file to your own project bootstrap
require_once 'bootstrap.php';

//configuring Doctrine command-line tools for the current project
//this file should be run one from command-line:
//php cli-config.php
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Infrastructure\Orm\Repositories\EntityManagerFactory;

// replace with mechanism to retrieve EntityManager in your app
$orm = new EntityManagerFactory();
$entityManager = $orm->getInstance();

return ConsoleRunner::createHelperSet($entityManager);

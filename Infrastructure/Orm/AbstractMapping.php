<?php

namespace Infrastructure\Orm;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;

abstract class AbstractMapping {

    /**
     * @var ClassMetadataBuilder
     */
    protected $builder;
    
    function __construct(ClassMetadata $metadata, ClassMetadataBuilder $builder=null) {
        
        if(!$builder) {
            $builder = new ClassMetadataBuilder($metadata);
        }
        $this->builder = $builder;
    } 
            
    function BuildMapping() {

        //$this->builder->setMappedSuperClass();

        $this->builder->createField('id', 'integer')
                ->isPrimaryKey()
                ->generatedValue()
                ->build();

        $this->builder->createField('createdAt', 'datetime')
                ->columnName('created_at')
                ->build();

        $this->builder->createField('updatedAt', 'datetime')
                ->columnName('updated_at')
                ->isVersionField()
                ->build();
    }
}

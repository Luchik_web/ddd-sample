<?php

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;
use Infrastructure\Orm\AbstractMapping;

class UserMapping extends AbstractMapping {

    function BuildMapping() {

        parent::BuildMapping();
                
        $this->builder->setTable('user');

        $this->builder->AddField('fname', 'string');
        $this->builder->AddField('sname', 'string');
        $this->builder->AddField('title', 'string');
        $this->builder->AddField('email', 'string');
        $this->builder->AddField('mobile', 'string');
        $this->builder->AddField('dob', 'datetime');
        $this->builder->AddField('last_logon_at', 'datetime');
        $this->builder->AddField('created_hash', 'datetime');
        $this->builder->AddField('sex', 'smallint');
        $this->builder->AddField('status', 'smallint');
        $this->builder->AddField('password_reset_hash', 'string');
    }
}


$mapping = new UserMapping($metadata);
$mapping->BuildMapping();
<?php

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;
use Infrastructure\Orm\AbstractMapping;

class OrganisationMapping extends AbstractMapping {
            
    function BuildMapping() {

        parent::BuildMapping();
        
        $this->builder->setTable('organisation');

        $this->builder->AddField('domain', 'string');
        $this->builder->AddField('name', 'string');
        $this->builder->AddField('privacy', 'smallint');
        $this->builder->AddField('budget', 'decimal');
        $this->builder->AddField('surplus', 'decimal');
        $this->builder->AddField('members_adding', 'smallint');
        $this->builder->AddField('status', 'smallint');
        $this->builder->AddField('category', 'smallint');        

    }
}

$mapping = new OrganisationMapping($metadata);
$mapping->BuildMapping();
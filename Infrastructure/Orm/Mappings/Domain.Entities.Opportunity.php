<?php

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;
use Infrastructure\Orm\AbstractMapping;

class OpportunityMapping extends AbstractMapping {
            
    function BuildMapping() {

        parent::BuildMapping();
        
        $this->builder->setTable('opportunity');

        $this->builder->AddField('title', 'string');
        $this->builder->AddField('description', 'string');
        $this->builder->AddField('enable', 'boolean');
        $this->builder->AddField('delete', 'boolean');
        $this->builder->AddField('status', 'smallint');
        $this->builder->AddField('is_enabled_geo_location', 'boolean');
        $this->builder->AddField('is_for_organisation_only', 'boolean');
        $this->builder->AddField('max_jobs', 'integer');
        $this->builder->AddField('recurring_days', 'smallint');
        $this->builder->AddField('cost', 'decimal');
        $this->builder->AddField('latitude', 'float');
        $this->builder->AddField('longitude', 'float');
        $this->builder->AddField('event_date', 'datetime');
        $this->builder->AddField('start_time', 'datetime');
        $this->builder->AddField('end_time', 'datetime');
        $this->builder->AddField('currency', 'smallint');

        $this->builder->createManyToOne('organisation', 'Organisation')
                ->addJoinColumn('organisation_id', 'id', FALSE)
                ->fetchLazy()
                ->build();

        $this->builder->createManyToOne('creator', 'User')
                ->addJoinColumn('creator_id', 'id', FALSE)
                ->fetchLazy()
                ->build();
    }
}

$mapping = new OpportunityMapping($metadata);
$mapping->BuildMapping();
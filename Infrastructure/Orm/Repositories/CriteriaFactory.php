<?php

namespace Infrastructure\Orm\Repositories;

use Doctrine\Common\Collections\Criteria;
use Domain\Specifications\ISpecificationTree;
use Domain\Specifications\EnumLogicalCondition;
use Domain\Specifications\EnumComparisonOperation;
use Domain\Specifications\IPropertyComparison;

use Doctrine\Common\Collections\ExpressionBuilder;
use Doctrine\Common\Collections\Expr\CompositeExpression;


class CriteriaFactory {
    
    private $builder;
    
    function __construct(ExpressionBuilder $expressionBuilder) {
        $this->builder = $expressionBuilder;
    }
 
    /**
     * Create Doctrine Criteria instance by domain specification
     * @param string $className name of class specification is applicable to
     * @param ISpecificationTree $spec
     * @return Criteria
     */
    function CreateCriteria($className, ISpecificationTree $spec){
        $expr = $this->CreateExpression($spec);
        $criteria = new Criteria($expr);
        
        return $criteria;
    }
    
    /**
     * Creates Doctrine COmpositeExpression object from Domain specification
     * 
     * @param ISpecificationTree $specification
     * @return CompositeExpression
     */
    protected function CreateExpression(ISpecificationTree $spec){
             
        if ($spec->getLeafExpression() != null) {
            
            return $this->CreateComparisonExpression($spec->getLeafExpression());
        }

        $lhs = $spec->getLeftSubtree();
        $rhs = $spec->getLeftSubtree();

        switch ($spec->getLogicalCondition()->key()) {

            case EnumLogicalCondition::_AND_ :
                return $this->builder->andX(
                                array(
                                    $this->CreateExpression($lhs),
                                    $this->CreateExpression($rhs)
                                )
                );

            case EnumLogicalCondition::_OR_ :
                return $this->builder->orX(
                                array(
                                    $this->CreateExpression($lhs),
                                    $this->CreateExpression($rhs)
                                )
                );

            default :
                throw new Exception('Unknown logical condition is used in the specification');
        }
    }
    
    protected function CreateComparisonExpression(IPropertyComparison $expr){
    
        function GetField(IPropertyComparison $expr){
            return $expr->getClass().'->'.$expr->getProperty();
        }
        
        switch ($expr->getPropertyOperation()->value()) {
            
            case EnumComparisonOperation::EQ :
                return $this->builder->eq(GetField($expr), $expr->getPropertyValues());
            case EnumComparisonOperation::NEQ :
                return $this->builder->neq(GetField($expr), $expr->getPropertyValues());
            case EnumComparisonOperation::GT :
                return $this->builder->gt(GetField($expr), $expr->getPropertyValues());
            case EnumComparisonOperation::GTE :
                return $this->builder->gte(GetField($expr), $expr->getPropertyValues());
            case EnumComparisonOperation::LT :
                return $this->builder->lt(GetField($expr), $expr->getPropertyValues());
            case EnumComparisonOperation::LTE :
                return $this->builder->lte(GetField($expr), $expr->getPropertyValues());
            case EnumComparisonOperation::IN :
                return $this->builder->in(GetField($expr), $expr->getPropertyValues());
            case EnumComparisonOperation::NOTIN :
                return $this->builder->notIn(GetField($expr), $expr->getPropertyValues());
            case EnumComparisonOperation::CONTAINS :
                return $this->builder->contains(GetField($expr), $expr->getPropertyValues());
            case EnumComparisonOperation::ISNULL :
                return $this->builder->isNull(GetField($expr));
            default:
                throw new Exception('Unknown field comparison');
        }
    }
    
 
}

<?php
namespace Infrastructure\Orm\Repositories;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\Mapping\Driver\PHPDriver;

class EntityManagerFactory {
    
    static private $em = null;
    
    /**
     * @return Doctrine\ORM\EntityManager 
     */
    function getInstance(){
        if(!self::$em){

            // the connection configuration
            $dbParams = array(
                'driver' => 'pdo_mysql',
                'user' => 'root',
                'password' => '123123',
                'dbname' => 'ddd-sample',
            );

//If $isDevMode is false, set then proxy classes have to be explicitly created through the command line.
            $isDevMode = true;

            $driver = new PHPDriver('Infrastructure/Orm/Mappings');

//$config = Setup::createXMLMetadataConfiguration($paths, $isDevMode);
            $config = Setup::createConfiguration($isDevMode);
            $config->setProxyDir('Infrastructure/Orm/Proxies');
            //$config->setEntityNamespaces(array ('Domain\Entities'));
            $config->setProxyNamespace('Domain\Entities');
            $config->setMetadataDriverImpl($driver);

            self::$em = EntityManager::create($dbParams, $config);

            $conn = self::$em->getConnection();
            $conn->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        }

        return self::$em;
    } 
  
}


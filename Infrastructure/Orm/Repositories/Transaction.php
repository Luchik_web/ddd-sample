<?php

namespace Infrastructure\Orm\Repositories;

use Doctrine\ORM\EntityManager;

use Domain\Repositories\ITransaction;



class Transaction implements ITransaction {
    
    
    private $entityManager;
    
    /**
     * 
     * @param \Doctrine\ORM\EntityManager $em
     */
    function __construct(EntityManager $em) {
        $this->entityManager = $em;
    }

    public function BeginTransaction() {
        $this->transaction = $this->entityManager->beginTransaction();
    }

    public function Commit() {
        $this->transaction = $this->entityManager->commit();        
    }

    public function Rollback() {
        $this->transaction = $this->entityManager->rollback();        
    }
}


<?php

namespace Infrastructure\Orm\Repositories;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Domain\Repositories\IGenericRepository;
use Domain\Entities\AbstractEntity;
use Domain\Specifications\ISpecificationTree;

class GenericRepository implements IGenericRepository {
    
    
    private $entityManager;
    private $criteriaFactory;
    
    /**
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @param CriteriaFactory $cf
     */
    function __construct(EntityManager $em, CriteriaFactory $cf) {
        $this->entityManager = $em;
        $this->criteriaFactory = $cf;
    }
    
    public function GetById($entityClass, $Id) {
        return $this->GetOrmRepository($entityClass)->find($Id);
    }

    public function GetBySpec($entityClass, ISpecificationTree $specification) {
        
        $criteria = $this->criteriaFactory->CreateCriteria($entityClass, $specification);
        
        return $this->GetOrmRepository($entityClass)->findBy($criteria);
    }

    /**
     * Adds or Updates entity to database
     * Do not pass detached entities (having Id<>0 but loaded into memory in different DB session)
     * since they would be added repeatedly.
     * @param \Domain\Entities\AbstractEntity $entity
     */
    public function Save(AbstractEntity $entity) {       
        $this->entityManager->persist($entity);
    }

    public function Remove(AbstractEntity $entity) {
        $this->entityManager->remove($entity);
    }

       
    /**
     * @param string $entityClass name of entity class
     * @return EntityRepository
     */
    protected function GetOrmRepository($entityClass){
        return $this->entityManager->getRepository($entityClass);
    }
}

